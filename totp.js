var express = require('express');
var app = express();
var fs = require("fs");

var jsSHA = require('./sha.js');

// below four function calls for generating Gooogle OTP using Secret Key
function dec2hex(s) {
    return (s < 15.5 ? '0' : '') + Math.round(s).toString(16);
}

function hex2dec(s) {
    return parseInt(s, 16);
}

function base32tohex(base32) {
    var base32chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
    var bits = "";
    var hex = "";

    for (var i = 0; i < base32.length; i++) {
        var val = base32chars.indexOf(base32.charAt(i).toUpperCase());
        bits += leftpad(val.toString(2), 5, '0');
    }

    for (var i = 0; i + 4 <= bits.length; i += 4) {
        var chunk = bits.substr(i, 4);
        hex = hex + parseInt(chunk, 2).toString(16);
    }
    return hex;
}

function leftpad(str, len, pad) {
    if (len + 1 >= str.length) {
        str = Array(len + 1 - str.length).join(pad) + str;
    }
    return str;
}

// Main method for Generating OTP Based on secret key paramenter
function googleAuthOtp(secretKey) {
    var key = base32tohex(secretKey);
    var epoch = Math.round(new Date().getTime() / 1000.0);
    var time = leftpad(dec2hex(Math.floor(epoch / 30)), 16, '0');
    var shaObj = new jsSHA("SHA-1", "HEX");
    shaObj.setHMACKey(key, "HEX");
    shaObj.update(time);
    var hmac = shaObj.getHMAC("HEX");
    var offset = hex2dec(hmac.substring(hmac.length - 1));
    var otp = (hex2dec(hmac.substr(offset * 2, 8)) & hex2dec('7fffffff')) + '';
    otp = (otp).substr(otp.length - 6, 6);
    return otp;
}



function otp() {

    // Secret Key Support User Production App
    let secretKey = 'ZHIAGA3XMYRTQAHQ';
    let otp = googleAuthOtp(secretKey);
    console.log('Genereated Production App OTP: ', otp);

    //Secret Key Gudivaka User Production App 
    secretKey = 'C6WEHTB4NXUK5BG3';
    otp = googleAuthOtp(secretKey);
    console.log('Genereated Gudivaka App OTP: ', otp);

    //Secret Key Gudivaka User Staging 
    secretKey = 'LVIEATNPKL4JKWS6';
    otp = googleAuthOtp(secretKey);
    console.log('Genereated Gudivaka Staging OTP: ', otp);
    
    //Secret Key Veera User Staging 
    secretKey = 'SPSZIZ53RBQTL3AQ';
    otp = googleAuthOtp(secretKey);
    console.log('Genereated Veera Veera Staging OTP: ', otp);

    //Secret Key Gudivaka User local 
    secretKey = 'EEG3FGDUMXQT5EJG';
    otp = googleAuthOtp(secretKey);
    console.log('Genereated Local tlscp  OTP: ', otp);
}

otp();